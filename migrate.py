#!/usr/bin/python
import psycopg2
import sys
import snowflake
import datetime

from os.path import join

con = None
con2 = None
filedir = "/var/www/elixireuploads/i/"


try:
    con = psycopg2.connect("host=127.0.0.1 dbname=elixire user=elixire password=elixire")
    con2 = psycopg2.connect("host=127.0.0.1 dbname=elixirev2 user=elixire password=elixire")
    get_cur = con.cursor()
    get_cur.execute("SELECT username,password,admin,creation from users")
    set_cur = con2.cursor()
    new_users = {}

    for user in get_cur.fetchall():
        print(f"Migrating user {user[0]}")
        user_timestamp = int(user[3].timestamp() * 1000)
        new_snowflake = snowflake._snowflake(user_timestamp)
        new_users[user[0]] = new_snowflake
        set_cur.execute(f"INSERT INTO users (user_id, username, password_hash, admin) VALUES ({new_snowflake}, '{user[0]}', '{user[1]}', {user[2]})")
        set_cur.execute(f"INSERT INTO limits (user_id) VALUES ({new_snowflake})")

    con2.commit()
    print(repr(new_users))
    get_cur.execute("SELECT filename,uploader,size,creation from images")

    for file in get_cur.fetchall():
        print(f"Migrating file {file[0]}")
        file_timestamp = int(file[3].timestamp() * 1000)
        new_snowflake = snowflake._snowflake(file_timestamp)
        
        uploader_id = new_users[file[1]]
        file_without_ext = file[0].split('.')[0]
        fullpath = join(filedir, file[0])
        set_cur.execute(f"INSERT INTO files (file_id, mimetype, filename, file_size, uploader, fspath, deleted) VALUES ({new_snowflake}, null, '{file_without_ext}', {file[2]}, {uploader_id}, '{fullpath}', false)")
    con2.commit()
except psycopg2.DatabaseError as e:
    if con2:
        con2.rollback()
    print(f'Error {e}')
    sys.exit(1)
finally:
    if con:
        con.close()
    if con2:
        con2.close()
